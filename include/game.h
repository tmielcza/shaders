#ifndef GAME_H
# define GAME_H

# include <windows.h>
# include "GL/glew.h"
# include "GL/wglew.h"
# include "GLFW/glfw3.h"

# define WINW	1024
# define WINH	768

# define PI		3.14159265359

# define TORAD(a)	(a * PI / 180.0)
# define POW2(x)	(x * x)
# define MAG(va)	(sqrt(POW2(va[0]) + POW2(va[1]) + POW2(va[2])))
# define ABS(x)		(x < 0 ? -x : x)

# define XV(err,res,str)	(x_void(err,res,str,__FILE__))
# define X(err,res,str)		(x_int(err,res,str,__FILE__))

enum			e_coord
{
	_x,
	_y,
	_z
};

enum			e_shaders
{
	_vpos,
	_vnorm,
	_vtex,
	_vtan,
	_vbinorm
};

enum			e_vbos
{
	_vbo_test,
	_vbo_test_norm,
	_vbo_test_texpos,
	_vbo_test_tan,
	_vbo_test_bitan,
	_vbo_nb
};

typedef struct	s_list
{
	void			*content;
	struct s_list	*next;
}				t_list;

typedef struct	s_glprog
{
	GLuint		prog;
	GLuint		vrtx;
	GLuint		frag;
}				t_glprog;

typedef struct	s_env
{
	GLFWwindow	*win;
	GLfloat		*mat_p;
	GLuint		*vbos;
	GLuint		texture;
	GLuint		loc_mv;
	GLuint		loc_n;
	t_glprog	prog;
	t_list		*mv_mat_stack;
}				t_env;

typedef struct	s_mat4
{
	GLfloat		mat[16];
}				t_mat4;

t_env		*g_env;

/*
**	env_init.c
*/
void		init_env(t_env *env);

/*
**	opengl_init.c
*/
void		init_gl(void);
int			init_win(t_env *env);
int			init_shader_env(t_env *env);

/*
**	view_utils.c
*/
float		*new_projection(float fovy, float aspect, float znear, float zfar);

/*
**	vec_utils.c
*/
void		normalize(GLfloat vec[3]);
void		getdir(GLfloat o[3], GLfloat dir[3]);
void		set_vec(GLfloat vec[3], GLfloat x, GLfloat y, GLfloat z);

/*
**	file_utils.c
*/
int			load_file(int *len, char **pdata, char *name);

/*
**	shader_utils.c (1 static)
*/
int			create_shaders_prog(t_glprog *prog, char *vrtx, char *frag);

/*
**	error.c
*/
void		*x_void(void *err, void *res, char *str, char *file);
int			x_int(int err, int res, char *str, char *file);

/*
**	list.c
*/
t_list		*add_link(t_list *next, void *content);
void		add_link_end(t_list **list, void *content);
void		del_link(t_list **link, void (*ft)(void *));
void		switch_link(t_list **src, t_list **dst);

/*
**	matrix.c
*/
void		push_matrix(void);
void		pop_matrix(void);
void		load_identity(void);
void		apply_matrix(void);

/*
**	matrix_utils.c
*/
void		mat_identity(t_mat4 *mat);
t_mat4		mat_mult(t_mat4 *A, t_mat4 *B);
t_mat4		new_mat_translate(GLfloat x, GLfloat y, GLfloat z);
t_mat4		new_mat_scale(GLfloat x, GLfloat y, GLfloat z);
t_mat4		new_mat_rotate(GLfloat a, GLfloat x, GLfloat y, GLfloat z);
t_mat4		new_mat_transpose(t_mat4 mat);
t_mat4		new_mat_inverse(t_mat4 mat);

/*
**	matrix_transf.c
*/
void		translate(GLfloat x, GLfloat y, GLfloat z);
void		scale(GLfloat x, GLfloat y, GLfloat z);
void		rotate(GLfloat a, GLfloat x, GLfloat y, GLfloat z);

/*
**	bmp.c (1 static)
*/
GLuint		load_bmp_tex(char *file);

#endif