#include <stdio.h>
#include <math.h> // A VIRER
#include "game.h"

void	light(t_env *env)
{
	GLuint	loc;

	loc = glGetUniformLocation(env->prog.prog, "Lights[0].pos");
	glUniform3f(loc, 0.0f, 0.0f, 5.0f);
	loc = glGetUniformLocation(env->prog.prog, "Lights[0].diffcolor");
	glUniform4f(loc, 1.0f, 1.0f, 1.0f, 1.0f);
	loc = glGetUniformLocation(env->prog.prog, "Lights[0].speccolor");
	glUniform4f(loc, 1.0f, 1.0f, 1.0f, 1.0f);
	loc = glGetUniformLocation(env->prog.prog, "Lights[0].ambcolor");
	glUniform4f(loc, 0.0f, 0.0f, 0.1f, 1.0f);
}

void	display_shit(t_env *env)
{
	static double	i = 0.0;

	light(env);
	
	push_matrix();
	rotate(30.0f * cos(TORAD(i)), 0.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnableVertexAttribArray(_vpos);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test]);
	glVertexAttribPointer(_vpos, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	glEnableVertexAttribArray(_vnorm);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test_norm]);
	glVertexAttribPointer(_vnorm, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	glEnableVertexAttribArray(_vtex);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test_texpos]);
	glVertexAttribPointer(_vtex, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);

	glEnableVertexAttribArray(_vtan);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test_tan]);
	glVertexAttribPointer(_vtan, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	glEnableVertexAttribArray(_vbinorm);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test_bitan]);
	glVertexAttribPointer(_vbinorm, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	apply_matrix();
	glDrawArrays(GL_QUADS, 0, 4);
	glFlush();
	pop_matrix();
	i += 0.2;
}

void	init_texture(t_env *env)
{
	GLuint	tex;
	GLuint	loc;

	glActiveTexture(GL_TEXTURE0);
	tex = load_bmp_tex("data\\Tex5.bmp");
	glActiveTexture(GL_TEXTURE1);
	tex = load_bmp_tex("data\\Tex4.bmp");

	loc = glGetUniformLocation(env->prog.prog, "Texture");
	glUniform1i(loc, 0);
	loc = glGetUniformLocation(env->prog.prog, "TextureBump");
	glUniform1i(loc, 1);
}

void	init_shit(t_env *env)
{
	static GLfloat	vertab[] =
	{
		-1.0, -1.0, 0.0,
		1.0, -1.0, 0.0,
		1.0, 1.0, 0.0,
		-1.0, 1.0, 0.0
	};
	static GLfloat	normtab[] =
	{
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0
	};
	static GLfloat	textab[] =
	{
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0
	};
	static GLfloat	tantab[] = 
	{
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0
	};
	static GLfloat	bitantab[] = 
	{
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0
	};

	translate(0.0F, 0.0F, -1.5F);

	glGenBuffers(_vbo_nb, env->vbos);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertab), vertab, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test_norm]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(normtab), normtab, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test_texpos]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(textab), textab, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test_tan]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(tantab), tantab, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test_bitan]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bitantab), bitantab, GL_STATIC_DRAW);

	init_texture(env);
}

void	za_gureto_ruppu_obu_za_deddo(t_env *env)
{
	while (glfwGetKey(env->win, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(env->win) == 0)
	{
		display_shit(env);
		glfwSwapBuffers(env->win);
		glfwPollEvents();
	}
}

int		main(void)
{
	t_env	env;

	init_env(&env);
	init_win(&env);
	init_gl();
	create_shaders_prog(&env.prog, "src\\shaders\\vert1.txt",
								"src\\shaders\\frag1.txt");

	init_shader_env(&env);

	push_matrix();

	init_shit(&env);

	za_gureto_ruppu_obu_za_deddo(&env);
	return (0);
}
