# README #

A test project for 3D vertex/fragment shaders with OpenGL.

It supports:

* Light (Diffuse, Ambient, Specular)
* Texture
* Bump Mapping
* Parallax Mapping

I used GLFW, GLEW and GSL.

### Setup ###

Works on Windows only.

Download and build with CodeBlocks.

Needs OpenGL 2.1

### Screenshots ###

![GLSL01.jpg](https://bitbucket.org/repo/oApE7a/images/2390961564-GLSL01.jpg)