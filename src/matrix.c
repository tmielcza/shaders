#include "game.h"

void	push_matrix(void)
{
	t_mat4	*new;
	t_list	**stack;

	stack = &g_env->mv_mat_stack;
	new = (t_mat4 *)malloc(sizeof(t_mat4));
	if (*stack)
		*new = *((t_mat4 *)(*stack)->content);
	else
		mat_identity(new);
	*stack = add_link(*stack, new);
}

void	pop_matrix(void)
{
	t_list	*stack;

	stack = g_env->mv_mat_stack;
	del_link(&g_env->mv_mat_stack, free);
}

void	load_identity(void)
{
	t_list	*stack;

	stack = g_env->mv_mat_stack;
	mat_identity((t_mat4 *)stack->content);
}

void	apply_matrix(void)
{
	t_mat4	norm;
	t_list	*stack;

	stack = g_env->mv_mat_stack;
	norm = *(t_mat4 *)stack->content;
	//Utile seulement s'il y a un redimensionnement non uniforme.
	norm = new_mat_inverse(norm);
	norm = new_mat_transpose(norm);
	glUniformMatrix4fv(g_env->loc_mv, 1, GL_TRUE, stack->content);
	glUniformMatrix4fv(g_env->loc_n, 1, GL_TRUE, (void *)&norm);
}