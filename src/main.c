#include <stdio.h>
#include <math.h> // A VIRER
#include "game.h"

void	display_shit(t_env *env)
{
	static float	i = 0.0;

	push_matrix();
	rotate(ABS(cos(TORAD(i)) * 180.0 - 90.0), 0.0F, 1.0F, 0.0F);
	glClear(GL_COLOR_BUFFER_BIT);
	glEnableVertexAttribArray(_vpos);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test]);
	glVertexAttribPointer(_vpos, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	glEnableVertexAttribArray(_vnorm);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test_norm]);
	glVertexAttribPointer(_vnorm, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	apply_matrix();
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glFlush();
	pop_matrix();
	i += 0.1;
}

void	init_shit(t_env *env)
{
	static GLfloat	vertab[] =
	{
		-1.0, -1.0, 0.0,
		1.0, -1.0, 0.0,
		0.0, 1.0, 0.0
	};
	static GLfloat	normtab[] =
	{
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0
	};

	translate(0.0F, 0.0F, -3.0F);

	glGenBuffers(2, env->vbos);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertab), vertab, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, env->vbos[_vbo_test_norm]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(normtab), normtab, GL_STATIC_DRAW);
}

void	za_gureto_ruppu_obu_za_deddo(t_env *env)
{
	while (glfwGetKey(env->win, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(env->win) == 0)
	{
		display_shit(env);
		glfwSwapBuffers(env->win);
		glfwPollEvents();
	}
}

void	light(t_env *env)
{
	GLuint	loc;

	loc = glGetUniformLocation(env->prog.prog, "LightPos");
	glUniform3f(loc, 0.0, 0.0, 0.0);
}

int		main(void)
{
	t_env	env;

	init_env(&env);
	init_win(&env);
//	init_gl();
	create_shaders_prog(&env.prog, "src\\shaders\\vert1.txt",
								"src\\shaders\\frag1.txt");

	init_shader_env(&env);

	light(&env);

	push_matrix();

	init_shit(&env);

	za_gureto_ruppu_obu_za_deddo(&env);
	return (0);
}
