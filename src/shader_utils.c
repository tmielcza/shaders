#include <stdio.h>
#include "game.h"

static int	compile_shader(int *id, char *file, GLenum flag)
{
	GLsizei		logsize;
	GLint		compile_status;
	int			datasize;
	char		*log;
	char		*shadata;

	load_file(&datasize, &shadata, file);
	*id = glCreateShader(flag);
	glShaderSource(*id, 1, &shadata, NULL);
	
	glCompileShader(*id);
	glGetShaderiv(*id, GL_COMPILE_STATUS, &compile_status);

	if (compile_status != GL_TRUE)
	{
		glGetShaderiv(*id, GL_INFO_LOG_LENGTH, &logsize);
		log = (char *)malloc(logsize + 1);
		memset(log, '\0', logsize + 1);
		glGetShaderInfoLog(*id, logsize, &logsize, log);
		fprintf(stderr, "Impossible de compiler le shader %s: \n%s", file, log);
		free(log);
		glDeleteShader(*id);
	}
	return (0);
}

int			create_shaders_prog(t_glprog *prog, char *vrtx, char *frag)
{
	compile_shader(&prog->vrtx, vrtx, GL_VERTEX_SHADER);
	compile_shader(&prog->frag, frag, GL_FRAGMENT_SHADER);
	prog->prog = glCreateProgram();
	glAttachShader(prog->prog, prog->vrtx);
	glAttachShader(prog->prog, prog->frag);
	glLinkProgram(prog->prog);
	glUseProgram(prog->prog);
	return (0);
}