#version 120

struct			Light
{
	vec3		pos;
	vec4		diffcolor;
	vec4		speccolor;
	vec4		ambcolor;
};

uniform	Light		Lights[5];
uniform sampler2D	Texture;
uniform sampler2D	TextureBump;

varying vec4	fPosition;
varying	vec3	fCamVec;
varying	vec3	fLightVec;
varying vec3	fNormal;
varying vec2	fTexCoord;

float	TexH(float x, float y)
{
	vec4	texel;
	vec2	Pos = vec2(x, y);

	texel = texture2D(TextureBump, Pos);
	return (texel.x * 2.0 - 1.0);
}

vec3	NormalMap(vec2 Coord)
{
	float	Z1;
	float	Z2;
	vec3	A;
	vec3	B;
	float	pxl = 1.0 / 64.0;

	Z1 = TexH(Coord.x, Coord.y) - TexH(Coord.x - pxl, Coord.y);
	Z2 = TexH(Coord.x, Coord.y) - TexH(Coord.x, Coord.y - pxl);
	A = vec3(1.0, 0.0, Z1);
	B = vec3(0.0, 1.0, Z2);
	return (normalize(cross(A, B)));
}

void main()
{
	int		i;
	float	Coef;
	vec3	LightVec;
	vec3	CamVec;
	vec3	ReflecVec;
	vec3	Normal;
	vec4	Diffuse;
	vec4	Specular;
	vec4	Ambient;
	vec4	FinalColor;
	vec4	TextureColor;
	vec2	TexCoord;

	LightVec = normalize(fLightVec);
	CamVec = normalize(fCamVec);
	i = 0;

	//Parallax Mapping	
	TexCoord = CamVec.xy * (TexH(fTexCoord.x, fTexCoord.y) * 0.1);
	TexCoord += fTexCoord;
	
	Normal = NormalMap(TexCoord);
	TextureColor = texture2D(Texture, fTexCoord);
	if (TextureColor == vec4(0.0, 0.0, 0.0, 1.0))
		return ;
	Coef = max(dot(Normal, LightVec), 0.0); // Ca aussi, eventuellement.
	Diffuse = Coef * Lights[i].diffcolor * TextureColor;
	ReflecVec = reflect(LightVec, Normal);
	Coef = max(dot(ReflecVec, CamVec), 0.0);
	Specular = pow(Coef, 180) * Lights[i].speccolor;
	Ambient = Lights[i].ambcolor;

	FinalColor = Diffuse + Specular + Ambient;

	gl_FragColor = FinalColor;
}