#include <math.h>
#include "game.h"

void	normalize(GLfloat vec[3])
{
	float	mag;

	mag = (float)MAG(vec);
	vec[0] /= mag;
	vec[1] /= mag;
	vec[2] /= mag;
}

void	getdir(GLfloat o[3], GLfloat dir[3])
{
	dir[0] -= o[0];
	dir[1] -= o[1];
	dir[2] -= o[2];
}

void	set_vec(GLfloat vec[3], GLfloat x, GLfloat y, GLfloat z)
{
	vec[_x] = x;
	vec[_y] = y;
	vec[_z] = z;
}