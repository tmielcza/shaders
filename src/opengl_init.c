#include <stdio.h>
#include "game.h"

void	init_gl(void)
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glClearDepth(1.0);
}

int		init_win(t_env *env)
{
	GLFWwindow *window;

	if (!glfwInit())
	{
		fprintf(stderr, "GLFW failed to init.\n");
		return (-1);
	}
	glfwWindowHint(GLFW_SAMPLES, 4);
 	window = glfwCreateWindow(WINW, WINH, "Tutorial 01", NULL, NULL);
	if (window == NULL)
	{
		fprintf( stderr, "Failed to open GLFW window.\n" );
		glfwTerminate();
		return (-1);
	}
	glfwMakeContextCurrent(window);
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		return (-1);
	}
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	env->win = window;
	return (0);
}

int		init_shader_env(t_env *env)
{
	GLuint	loc;

	env->loc_mv = glGetUniformLocation(env->prog.prog, "uModelViewMatrix");
	env->loc_n = glGetUniformLocation(env->prog.prog, "uNormalMatrix");
	loc = glGetUniformLocation(env->prog.prog, "uProjectionMatrix");
	glUniformMatrix4fv(loc, 1, GL_TRUE, env->mat_p);
	return (0);
}