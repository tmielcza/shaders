#include "game.h"

void	translate(GLfloat x, GLfloat y, GLfloat z)
{
	t_mat4	new;
	t_mat4	trans;
	t_list	*stack;

	stack = g_env->mv_mat_stack;
	trans = new_mat_translate(x, y, z);
	new = mat_mult(stack->content, &trans);
	*(t_mat4 *)stack->content = new;
}

void	scale(GLfloat x, GLfloat y, GLfloat z)
{
	t_mat4	new;
	t_mat4	scale;
	t_list	*stack;

	stack = g_env->mv_mat_stack;
	scale = new_mat_scale(x, y, z);
	new = mat_mult(stack->content, &scale);
	*(t_mat4 *)stack->content = new;
}

void	rotate(GLfloat a, GLfloat x, GLfloat y, GLfloat z)
{
	t_mat4	new;
	t_mat4	scale;
	t_list	*stack;
	GLfloat	vec[3];

	set_vec(vec, x, y, z);
	normalize(vec);
	stack = g_env->mv_mat_stack;
	scale = new_mat_rotate((float)TORAD(a), vec[_x], vec[_y], vec[_z]);
	new = mat_mult(stack->content, &scale);
	*(t_mat4 *)stack->content = new;
}
