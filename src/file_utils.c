#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <io.h>
#include <errno.h>
#include <string.h>
#include <share.h>
#include <sys/stat.h>

int		load_file(int *len, char **pdata, char *name)
{
	char	*data;
	int		fd;
	int		l;

	if (_sopen_s(&fd, name, O_RDONLY, _SH_DENYWR, _S_IWRITE))
	{
		printf("Bad file descriptor.\n");
		return (1);
	}
	l = _lseek(fd, 0, SEEK_END);
	_lseek(fd, 0, SEEK_SET);
	data = (char *)malloc(l * sizeof(char) + 1);
	l = _read(fd, data, l);
	memset(data + l, '\0', 1);
	*pdata = data;
	*len = l;
	_close(fd);
	return (0);
}
