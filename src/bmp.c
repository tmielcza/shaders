#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <io.h>
#include <errno.h>
#include <string.h>
#include <share.h>
#include <sys/stat.h>
#include "game.h"

static GLuint	new_tex(int w, int h, char *data)
{
	GLuint	id;

	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h,
				0, GL_BGR, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	return (id);
}

GLuint			load_bmp_tex(char *file)
{
	int				fd;
	char			*data;
	int				*offset;
	int				*size;
	GLuint			tex;

	if (_sopen_s(&fd, file, O_RDONLY, _SH_DENYWR, _S_IWRITE))
	{
		printf("Bad file descriptor.\n");
		return (0);
	}
	data = (char *)malloc(14 * sizeof(char));
	if (!_read(fd, data, 14))
		return (0);
	offset = (int *)(data + 10);
	size = (int *)(data + 2);
//	free(data); Free ailleurs, je sais pas ou.
	data = (char *)malloc((*size - 14) * sizeof(char));
	if (!_read(fd, data, *size - 14))
		return (0);
	tex = new_tex(*(int *)(data + 4), *(int *)(data + 8), &data[*offset - 14]);
	free(data);
	_close(fd);
	return (tex);
}
