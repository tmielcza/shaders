#include <math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>
#include "game.h"

t_mat4	mat_mult(t_mat4 *A, t_mat4 *B)
{
	t_mat4	C;
	int		i;

	i = 0;
	while (i < 16)
	{
		C.mat[i] = A->mat[i / 4 * 4 + 0] * B->mat[i % 4 + 0];
		C.mat[i] += A->mat[i / 4 * 4 + 1] * B->mat[i % 4 + 4];
		C.mat[i] += A->mat[i / 4 * 4 + 2] * B->mat[i % 4 + 8];
		C.mat[i] += A->mat[i / 4 * 4 + 3] * B->mat[i % 4 + 12];
		i++;
	}
	return (C);
}

void	mat_identity(t_mat4 *mat)
{
	memset(mat, '\0', 16 * sizeof(float));
	mat->mat[0] = 1;
	mat->mat[5] = 1;
	mat->mat[10] = 1;
	mat->mat[15] = 1;
}

t_mat4	new_mat_translate(GLfloat x, GLfloat y, GLfloat z)
{
	t_mat4	mat;

	mat_identity(&mat);
	mat.mat[3] = x;
	mat.mat[7] = y;
	mat.mat[11] = z;
	return (mat);
}

t_mat4	new_mat_scale(GLfloat x, GLfloat y, GLfloat z)
{
	t_mat4	mat;

	mat_identity(&mat);
	mat.mat[0] = x;
	mat.mat[5] = y;
	mat.mat[10] = z;
	return (mat);
}

t_mat4	new_mat_rotate(GLfloat a, GLfloat x, GLfloat y, GLfloat z)
{
	t_mat4	mat;
	GLfloat	c;
	GLfloat	s;

	c = (float)cos(a);
	s = (float)sin(a);
	mat_identity(&mat);
	mat.mat[0] = POW2(x) + (1 - POW2(x)) * c;
	mat.mat[1] = x * y * (1 - c) - z * s;
	mat.mat[2] = x * z * (1 - c) + y * s;
	mat.mat[4] = x * y * (1 - c) + z * s;
	mat.mat[5] = POW2(y) + (1 - POW2(y)) * c;
	mat.mat[6] = y * z * (1 - c) - x * s;
	mat.mat[8] = x * z * (1 - c) - y * s;
	mat.mat[9] = y * z * (1 - c) + x * s;
	mat.mat[10] = POW2(z) + (1 - POW2(z)) * c;
	return (mat);
}

t_mat4	new_mat_transpose(t_mat4 mat)
{
	int			i;
	gsl_matrix	*m;

	m = gsl_matrix_alloc(4, 4);
	i = 0;
	while (i < 16)
	{
		m->data[i] = mat.mat[i];
		i++;
	}
	gsl_matrix_transpose(m);
	i = 0;
	while (i < 16)
	{
		mat.mat[i] = (float)m->data[i];
		i++;
	}
	gsl_matrix_free(m);
	return (mat);
}

t_mat4	new_mat_inverse(t_mat4 mat)
{
	int 			s;
	int				i;
	gsl_matrix		*mat_gsl;
	gsl_matrix		*inverse;
	gsl_permutation	*perm;

	mat_gsl = gsl_matrix_alloc(4, 4);
	inverse = gsl_matrix_alloc(4, 4);
	perm = gsl_permutation_alloc(4);
	i = 0;
	while (i < 16)
	{
		mat_gsl->data[i] = mat.mat[i];
		i++;
	}
	gsl_linalg_LU_decomp(mat_gsl, perm, &s);
	gsl_linalg_LU_invert(mat_gsl, perm, inverse);
	while (--i >= 0)
	{
		mat.mat[i] = (float)inverse->data[i];
	}
	gsl_matrix_free(mat_gsl);
	gsl_matrix_free(inverse);
	return(mat);
}