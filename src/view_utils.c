#include <stdlib.h>
#include <math.h>
#include <windows.h>
#include "game.h"

float		*new_projection(float fovy, float aspect, float znear, float zfar)
{
	float	*m;
	float	xmax;
	float	ymax;

	m = (float *)malloc(16 * sizeof(float));
	SecureZeroMemory(m, 16 * sizeof(float));
	ymax = znear * (float)tan(TORAD(fovy));
	xmax = ymax * aspect;
	m[0] = (2.0f * znear) / (2.0f * xmax);
//	m[0] = (2.0f * znear) / (2.0f * xmax) / aspect; ???
	m[5] = (2.0f * znear) / (2.0f * ymax);
	m[10] = (-zfar - znear) / (zfar - znear);
	m[11] = -1.0f;
	m[14] = (-(2.0f * znear) * zfar) / (zfar - znear);
	return (m);
}
