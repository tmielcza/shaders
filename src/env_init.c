#include <stdlib.h>
#include "game.h"

void		init_env(t_env *env)
{
	SecureZeroMemory(env, sizeof(*env));
	env->mat_p = new_projection(45.0f, 1024.0f / 768.0f, 1.0f, 100.0f);
	env->vbos = (GLuint *)malloc(_vbo_nb * sizeof(GLuint));
	g_env = env;
}
